class Post < ActiveRecord::Base
	#Diz que um post tem varios comentarios e ao ser deletado, deleta os comentarios junto
	has_many :comments, dependent: :destroy
	#Valida, exigindo informacoes nos campos title e body
	validates_presence_of :title
	validates_presence_of :body
end
