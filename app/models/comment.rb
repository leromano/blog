class Comment < ActiveRecord::Base
	#Diz que um comentario pertence a um unico post
	belongs_to :post
	#Valida, exigindo informacoes nos campos post_id e body
	validates_presence_of :post_id
	validates_presence_of :body
end
